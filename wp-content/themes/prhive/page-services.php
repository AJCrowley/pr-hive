<?php
/*
 Template Name: Services Page
*/
?>
<?php remove_filter ('the_content', 'wpautop'); ?>
<?php get_header(); ?>
<?php
$menu_items = get_menu('services-menu');
foreach((array)$menu_items as $key => $item) {
	$menu_items[$key]->url = $item->url;
}
$bar_colour = 'bg-' . get_post_meta($post->ID, 'Top bar colour', true);
?>
<div class="topbar <?=$bar_colour?>"></div>
<div class="row">
	<div class="small-12 medium-4 columns small-order-2 medium-order-1">
		<h3>Services</h3>
		<ul class="vertical menu svcmenu">
			<?php
				foreach((array)$menu_items as $key => $item) {
					echo '<li><a ';
					if(get_permalink($post->ID) == $item->url) {
						echo 'class="active" ';
					}
					echo 'href="' . $item->url . '">' . $item->title . '</a></li>';
				}
			?>
		</ul>
	</div>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php the_content(); ?>
	<?php endwhile; ?>
	<?php endif; ?>
</div>
<?php get_footer(); ?>
