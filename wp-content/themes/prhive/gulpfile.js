var del 		 = require('del');
var gulp 		 = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var compass		 = require('gulp-compass');
var debug 		 = require('gulp-debug');
var imagemin 	 = require('gulp-imagemin');
var notify		 = require('gulp-notify');
var plumber		 = require('gulp-plumber');
var sourcemaps	 = require('gulp-sourcemaps');
var watch		 = require('gulp-watch');
var path		 = require('path');

var paths = {
	sass		 : ['./sass/'],
	cssTemp		 : ['./css/'],
	cssOutput	 : './styles/',
	imageDest 	 : './img/',
	imageSrc	 : './src_img/'
};

var notifyInfo = {
	title: 'Gulp',
	icon: path.join(__dirname, 'gulp.png')
};

var plumberErrorHandler = {
	errorHandler: notify.onError({
		title: notifyInfo.title,
		icon: notifyInfo.icon,
		message: "Error: <%= error.message %>"
	})
};

gulp.task('build', ['images', 'sass']);

gulp.task('sass', ['compass', 'prefix', 'clean:temp']);

gulp.task('compass', function() {
	return gulp.src(paths.sass + '**/*.scss')
		.pipe(debug({title: 'Processing:'}))
		.pipe(plumber(plumberErrorHandler))
		.pipe(compass({
			config_file	: './config.rb'
		}))
		.pipe(gulp.dest(paths.cssOutput));
});

gulp.task('prefix', ['compass'], function() {
	return gulp.src(paths.cssTemp + '**/*.css')
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(autoprefixer({
			browsers: ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']
		}))
		.pipe(sourcemaps.write('.')) // lose the '.' to write an inline sourcemap instead of ext file
		.pipe(gulp.dest(paths.cssOutput));
});

gulp.task('images', function() {
	return gulp.src(paths.imageSrc + '**/*')
        .pipe(imagemin())
        .pipe(gulp.dest(paths.imageDest));
});

gulp.task('clean:temp', ['prefix'], function() {
	return del(paths.cssTemp);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass + '**/*.scss', ['sass']);
});