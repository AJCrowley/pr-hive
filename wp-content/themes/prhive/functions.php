<?php
/*
Author: Kris McCann
URL: http://8pi.ca
*/

// set up CSS and JS
function prhive_scripts() {
	wp_enqueue_style( 'style.css', get_stylesheet_uri() );
	wp_register_script(
		'jquery',
		get_stylesheet_directory_uri() . '/js/lib/jquery.min.js',
		false,
		'1.11.3',
		true
	);
	wp_register_script(
		'what-input',
		get_stylesheet_directory_uri() . '/js/lib/what-input.min.js',
		false,
		'1.1.3',
		true
	);
	wp_register_script(
		'foundation',
		get_stylesheet_directory_uri() . '/js/lib/foundation.min.js',
		false,
		'6.0',
		true
	);
	wp_register_script(
		'waypoints',
		get_stylesheet_directory_uri() . '/js/lib/jquery.waypoints.min.js',
		false,
		'1.0',
		true
	);
	wp_register_script(
		'odometer',
		get_stylesheet_directory_uri() . '/js/lib/odometer.min.js',
		false,
		'1.0',
		true
	);
	wp_register_script(
		'matchHeight',
		get_stylesheet_directory_uri() . '/js/lib/jquery.matchHeight.js',
		false,
		'1.0',
		true
	);
	wp_enqueue_script(
		'app',
		get_stylesheet_directory_uri() . '/js/app.js',
		array('jquery', 'what-input', 'foundation', 'waypoints', 'odometer', 'matchHeight'),
		'1.0',
		true
	);
}
add_action( 'wp_enqueue_scripts', 'prhive_scripts' );

// return menu items in array by name
function get_menu($menu_name) {
	if(($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
		$menu = wp_get_nav_menu_object($locations[$menu_name]);
		$menu_items = wp_get_nav_menu_items($menu->term_id);
	}
	return $menu_items;
}

// enable upload of SVG graphics
function custom_upload_mimes ( $existing_mimes=array() ) {
	$existing_mimes['svg'] = 'mime/type';
	return $existing_mimes;
}
add_filter('upload_mimes', 'custom_upload_mimes');

// turn off admin bar from front end
add_filter('show_admin_bar', '__return_false');

// register navigation
function register_navigation() {
	register_nav_menu('main-menu', __('Main Menu', 'main-menu'));
	register_nav_menu('services-menu', __('Services Menu'), 'services-menu');
}
add_action('init', 'register_navigation');

// turn on featured image support
add_theme_support('post-thumbnails');

// add Display Order column header in admin
function custom_posts_table_head($columns) {
    $columns['Display Order']  = 'Display Order';
    return $columns;
}
add_filter('manage_posts_columns', 'custom_posts_table_head');

// add data for Display Order column in admin
function custom_posts_table_content( $column_name, $post_id ) {
    if( $column_name == 'Display Order' ) {
        echo get_post_meta($post_id, 'Display Order', true);
    }
}
add_action('manage_posts_custom_column', 'custom_posts_table_content', 10, 2);

// enable categories for pages
function page_categories() {
  register_taxonomy_for_object_type('category', 'page');
}
add_action('init', 'page_categories');

function autop_admin_menu() {
	add_menu_page('AutoP Options', 'AutoP Options', 'manage_options', 'autop-options.php', 'autop_admin_page', 'dashicons-media-code', 6);
}
add_action('admin_menu', 'autop_admin_menu');

function autop_admin_page() {
	if(isset($_POST['_wpnonce'])) {
		if(isset($_POST['autop_disabled'])) {
			update_option('autop_disabled', $_POST['autop_disabled'] == '1' ? 1 : 0);
		} else {
			update_option('autop_disabled', 0);
		}
	}
	?>
	<div class="wrap">
		<h2>AutoP Options</h2>
		<form method="post">
			<?php settings_fields('autop-settings-group'); ?>
			<?php do_settings_sections('autop-settings-group'); ?>
			<table class="form-table">
				<tr valign="top">
					<th scope="row">Disable Wordpress Auto Paragraphs</th>
					<td><input type="checkbox" name="autop_disabled" <?=get_option('autop_disabled') == '1' ? 'checked' : '';?> value="1"></td>
				</tr>
			</table>
			<?php submit_button(); ?>
		</form>
	<?php
}

// disable paragraph auto-insertion by wordpress
function disable_autop( $content )
{
	if(get_option('autop_disabled') == '1') {
		remove_filter( 'the_content', 'wpautop' );
	}
	return $content;
}
add_filter('the_content', 'disable_autop', 0);

// return get_tempalte_part as a return value
function load_template_part($template_name, $part_name=null) {
  ob_start();
  get_template_part($template_name, $part_name);
  $var = ob_get_contents();
  ob_end_clean();
  return $var;
}

// display bio template
function get_bio_section_shortcode($atts) {

	global $post;
	$output = load_template_part('templates/bio');
	return $output;
}
add_shortcode( 'get_bio_section', 'get_bio_section_shortcode' );


?>