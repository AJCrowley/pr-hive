                    <footer id="footer">
                        <div class="row">
                            <div class="small-12 columns social single">
                                <a href="http://facebook.com/prhive"><i class="fi-social-facebook larger"></i></a>
                                <a href="http://twitter.com/theprhive"><i class="fi-social-twitter larger"></i></a>
                                <a href="http://instagram.com/prhive"><i class="fi-social-instagram larger"></i></a>
                                <a href="https://ca.linkedin.com/in/robyn-mcisaac-mpr-203a9b2b"><i class="fi-social-linkedin larger"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 medium-4 colums">
				2085 Maitland St., Suite 100<br>
				Halifax, NS B3K 2Z8<br>
                                902 440 1551<br><br>

                                Robyn: <a href="mailto:robyn@prhive.ca">robyn@prhive.ca</a> |  902 440 1551<br>
                                Heather: <a href="mailto:heather@prhive.ca">heather@prhive.ca</a> |  902 488 2892

                            </div>
                            <div class="small-12 medium-4 colums">
                                <a href="http://facebook.com/prhive">facebook.com/prhive</a><br>
                                <a href="http://twitter.com/theprhive">twitter: @theprhive</a><br>
                                <a href="http://instagram.com/prhive">instagram: @prhive</a><br>
                                <a href="https://ca.linkedin.com/in/robyn-mcisaac-mpr-203a9b2b">linkedin: Robyn McIsaac</a><br>
                                <a href="https://www.linkedin.com/in/heather-hanson-aa064937">linkedin: Heather Hanson</a>

                            </div>
                            <div class="small-12 medium-4 colums">
                                Relationships matter<br>
                                in business<br>
                                And we help you<br>
                                build extraordinary ones.
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns single">
                                <!--A Division of Infuse Public Relations-->
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
        <?php wp_footer(); ?>
    </body>
</html>
