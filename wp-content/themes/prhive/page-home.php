<?php
/*
 Template Name: Home Page
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php 
    // remove_filter ('the_content', 'wpautop'); 
    // the_content();
    $post_content = get_the_content();
    $post_content = apply_filters( 'the_content', $post_content );
  ?>

  <?php print wpautop( $post_content ); ?>
  <?php endwhile; ?>

<?php endif; ?>
<?php get_footer(); ?>
