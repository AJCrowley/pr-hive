<?php
// build menu into array
$menu_items = get_menu('main');
foreach((array)$menu_items as $key => $item) {
	if(!is_front_page()) {
		$menu_items[$key]->url = '/' . $item->url;
	}
}
?>
<!doctype html>
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>><!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php wp_title(''); ?></title>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->

		<link rel="stylesheet" href="//use.fontawesome.com/releases/v5.0.10/css/brands.css">
		<link rel="stylesheet" href="//use.fontawesome.com/releases/v5.0.10/css/fontawesome.css">

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
		<div class="off-canvas-wrapper">
			<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
				<div class="off-canvas position-left" id="offCanvas" data-off-canvas>
					<ul class="mobile-ofc vertical menu" data-magellan>
						<?php
							foreach((array)$menu_items as $key => $item) {
								echo '<li><a data-toggle="offCanvas" href="' . $item->url . '">' . $item->title . '</a></li>';
							}
						?>
					</ul>
				</div>
				<div class="off-canvas-content" data-off-canvas-content>
					<header class="main top-bar" id="home" data-topbar role="navigation" data-magellan-target="home">
						<nav>
							<div class="logo">
								<a href="/"></a>
							</div>

							<div data-sticky-container class="show-for-medium" class="sticky-container">
								<div class="sticky-menu is-anchored is-at-top" data-sticky data-margin-top="0" data-margin-bottom="0" data-top-anchor="main_nav_0" data-btm-anchor="footer:bottom">
									<nav>
										<ul class="menu" data-magellan>
											<?php
												foreach((array)$menu_items as $key => $item) {
													echo '<li><a id="main_nav_' . $key . '" href="' . $item->url . '">' . $item->title . '</a></li>';
												}
											?>
										</ul>
									</nav>
								</div>
							</div>
							<span class="responsive-menu top-bar-right hide-for-medium float-right">
								<button class="menu-icon" type="button" data-toggle="offCanvas"></button>
							</span>
						</nav>
					</header>

