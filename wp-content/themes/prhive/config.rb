require 'compass/import-once/activate'

css_dir = "/styles"
sass_dir = "/sass"
images_dir = "/img"
javascripts_dir = "/js"

output_style = :compressed
relative_assets = true
sass_options = {:sourcemap => true}
preferred_syntax = :sass