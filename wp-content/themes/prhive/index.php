<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<section class="bg-white fg-brown">
		<div>
			<h2 class="fg-maroon page-title"><?php the_title(); ?></h2>
			<?php the_content(); ?>
		</div>
	</section>
<?php endwhile; ?>
<?php else : ?>
	<section class="bg-white fg-brown">
		<div>
			No posts found!
		</div>
	</section>
<?php endif; ?>
<?php get_footer(); ?>
