<?php
  /* Bio Section Template */
  global $post;
  $bio = get_field('bio_content');
?>

<div class="bio">

  <?php if ( is_array($bio) ): foreach( $bio as $item ): ?>

  <div class="bio__item">
    
    <div class="bio__headshot">
      <?php $image = $item['headshot']; ?>
      <img class="portrait" src="<?php print $image['sizes']['large']; ?>" alt="<?php print $item['name']; ?>" />
    </div>

    <h2 class="bio__name  fg-pink">About <?php print $item['name']; ?></h2>
    
    <div class="bio__description">
      <?php print wpautop( $item['description'] ); ?>
    </div>

    <div class="bio__experience">
      <h5 class="bio__experience__title">Past positions include:</h5>
      
      <?php print wpautop( $item['experience'] ); ?>
      
      <h5 class="bio__experience__linkedin"><a class="fg-white" target="_blank" href="<?php print $item['linkedin_url']; ?>"><i class="fab fa-linkedin-in"></i><span>View LinkedIn Profile</span></a></h5>

    </div>

  </div>

  <?php endforeach; endif; ?>

</div>