<?php
/*
Plugin Name: Foundation Orbit Carousel
Plugin URI: http://8pi.ca
Description: Quick plugin to build an Orbit carousel in Foundation 6
Version: 1.0
Author: Kris McCann
Author URI: http://8pi.ca
*/

class FoundationOrbit {
	public function __construct()
    {
        add_shortcode('orbit-carousel', array($this, 'orbit_func'));
    }

	public function orbit_func($atts) {
		$posts = $this->getPosts($atts['cat']);
		return $this->render($posts);
	}

	protected function getPosts($category) {
		return get_posts(array('category_name'=>$category));
	}

	protected function render($posts) {
		$output = $this->loadTemplate('/foundation-orbit-tpl.php');
		$tpl_slide = $this->loadTemplate('/foundation-orbit-tpl-slide.php');
		$tpl_prevnext = $this->loadTemplate('/foundation-orbit-tpl-prevnext.php');
		$tpl_bullets = $this->loadTemplate('/foundation-orbit-tpl-bullets.php');
		$tpl_bullet = $this->loadTemplate('/foundation-orbit-tpl-bullet.php');
		$active = 'is-active';
		$bullets = '';
		$slides = '';
		foreach($posts as $key=>$post) {
			$curBullet = str_replace('{{active}}', $active, $tpl_bullet);
			$curBullet = str_replace('{{index}}', $key, $curBullet);
			$curBullet = str_replace('{{caption}}', $post->title, $curBullet);
			$bullets .= $curBullet;
			$curSlide = str_replace('{{active}}', $active, $tpl_slide);
			$curSlide = str_replace('{{index}}', $key, $curSlide);
			$curSlide = str_replace('{{image}}', wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail')[0], $curSlide);
			$curSlide = str_replace('{{content}}', $post->post_content, $curSlide);
			$slides .= $curSlide;
			$active = '';
		}
		if(count($posts) < 2) {
			$tpl_prevnext = '';
			$bullets = '';
			$autoplay = 'false';
		} else {
			$autoplay = 'true';
			$bullets = str_replace('{{bullets}}', $bullets, $tpl_bullets);
		}
		$output = str_replace('{{autoplay}}', $autoplay, $output);
		$output = str_replace('{{prevNext}}', $tpl_prevnext, $output);
		$output = str_replace('{{slides}}', $slides, $output);
		$output = str_replace('{{bullets}}', $bullets, $output);
		return $output;
	}

	protected function loadTemplate($file) {
		return file_get_contents(plugin_dir_url( __FILE__ ) . $file);
	}
 }

 $foundationOrbit = new FoundationOrbit();