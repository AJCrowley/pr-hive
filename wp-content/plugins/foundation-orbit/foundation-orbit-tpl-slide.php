<li class="orbit-slide {{active}}" data-slide="{{index}}" style="background-image: url({{image}});">
	<figcaption class="orbit-caption">
		<div>
			{{content}}
		</div>
	</figcaption>
</li>