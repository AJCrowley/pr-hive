<div id="carousel" class="orbit" role="region" data-orbit data-options="autoPlay: {{autoplay}}; swipe: false;">
	<ul class="orbit-container" tabindex="0">
		{{prevNext}}
		{{slides}}
	</ul>
	{{bullets}}
</div>
